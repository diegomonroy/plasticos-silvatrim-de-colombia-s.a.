// JavaScript Document

/* ************************************************************************************************************************

PLÁSTICOS SILVATRIM DE COLOMBIA S.A.

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {

	/* Foundation */

	jQuery( '#nombre, #correo, #mensaje' ).foundation( 'destroy' );

});