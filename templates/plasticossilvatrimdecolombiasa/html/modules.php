<?php

/* ************************************************************************************************************************

PLÁSTICOS SILVATRIM DE COLOMBIA S.A.

File:			modules.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

/*
 * Module chrome case 1
 */
function modChrome_case1( $module ) {
	echo $module->content;
}